# By Wilbert Belandria
# Lista de Palabras para el Anagrama
word_list = ['heros', 'horse', 'shore', 'standard', 'casa', 'asac']

def anagrams(word, word_list)
# Mapear el array a hash
		words = Hash[word_list.map { |x| [x.upcase, x.chars.sort.join.upcase] }]
# Agrupar elementos
		words = words.group_by { |key, value| words[key] }
# Removiendo los valores
		words.each_pair do |key, value|
			words[key] = value.transpose.delete_at(0)
		end
# Revisar si existe una palabra en la lista
		puts word.chars
		if words[word.chars.sort.join.upcase] == nil
			puts "Lo sentimos no encontramos anagrama para su palabra: #{word}."
		else
			puts words[word.chars.sort.join.upcase]
		end
end

puts 'Enter a Word:'

anagrams(gets.chomp, word_list)