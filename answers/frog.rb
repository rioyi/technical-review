# By Wilbert Belandria
# Ejercicio 2

def frog_jump(x, y, d)
# Contador de Saltos
  jump = 0
  if x <= y
    if x == y
      jump = 1
    else
      while x < y
# Contamos los saltos y sumamos la distancia
        x += d
        jump += 1
      end
    end
    puts "The minimal number of jumps that the small frog must perform to reach its target is  #{jump}"
  else
    puts 'Point x must be less than point y'
  end
end

puts 'Enter the exit point X:'
x = gets.chomp.to_i
puts 'Enter the arrival point Y:'
y = gets.chomp.to_i
puts 'Enter the number of jumps per turn D:'
d = gets.chomp.to_i

frog_jump(x,y,d)
