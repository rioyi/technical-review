
var Calendario = function(modelo, date){
  modelo?this.Modelo=modelo:this.Modelo={};
  this.Hoy = new Date();
  this.seleccionado = this.Hoy
  this.Hoy.Mes = this.Hoy.getMonth();
  this.Hoy.Ano = this.Hoy.getFullYear();
  if(date){this.seleccionado = date}
  this.seleccionado.Mes = this.seleccionado.getMonth();
  this.seleccionado.Ano = this.seleccionado.getFullYear();
  this.seleccionado.Dias = new Date(this.seleccionado.Ano, (this.seleccionado.Mes + 1), 0).getDate();
  this.seleccionado.PrimerDia = new Date(this.seleccionado.Ano, (this.seleccionado.Mes), 1).getDay();
  this.seleccionado.UltimoDia = new Date(this.seleccionado.Ano, (this.seleccionado.Mes + 1), 0).getDay();
  this.Previo = new Date(this.seleccionado.Ano, (this.seleccionado.Mes - 1), 1);
  if(this.seleccionado.Mes==0){this.Previo = new Date(this.seleccionado.Ano-1, 11, 1);}
  this.Previo.Dias = new Date(this.Previo.getFullYear(), (this.Previo.getMonth() + 1), 0).getDate();
};

function crearCalendario(calendario, elemento, ajuste){
  //Limpia y pone el otro calendario
  if(typeof ajuste !== 'undefined'){
    var nueFacha = new Date(calendario.seleccionado.Ano, calendario.seleccionado.Mes + ajuste, 1);
    calendario = new Calendario(calendario.Modelo, nueFacha);
    elemento.innerHTML = '';
  }
  
  var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"];

  var mainSeccion = document.createElement('div');
  mainSeccion.className += "cld-main";

  function AgregarFecha()
  {
      var fecha = document.createElement('div');
      fecha.className += "cld-fecha";  
        var flecha_izquierda = document.createElement('div');
        //Ir al mes anterior
        flecha_izquierda.className += " cld-flecha_izquierda cld-nav";
        flecha_izquierda.addEventListener('click', function(){crearCalendario(calendario, elemento, -1);} );

        flecha_izquierda.innerHTML = '<svg height="15" width="15" viewBox="0 0 75 100" fill="rgba(0,0,0,0.5)"><polyline points="0,50 75,0 75,100"></polyline></svg>';
        fecha.appendChild(flecha_izquierda);
      
      var Hoy = document.createElement('div');
      //Mostrar Mes y Año actual
      Hoy.className += ' hoy';
      Hoy.innerHTML = meses[calendario.seleccionado.Mes] + ", " + calendario.seleccionado.Ano;
      fecha.appendChild(Hoy);
        //Ir al mes siguiente
        var flecha_derecha = document.createElement('div');
        flecha_derecha.className += " cld-flecha_derecha cld-nav";
        flecha_derecha.addEventListener('click', function(){crearCalendario(calendario, elemento, 1);} );
        flecha_derecha.innerHTML = '<svg height="15" width="15" viewBox="0 0 75 100" fill="rgba(0,0,0,0.5)"><polyline points="0,0 75,50 0,100"></polyline></svg>';
        fecha.appendChild(flecha_derecha);      
        mainSeccion.appendChild(fecha);
  }


  function AgregarDiaSemana(){
    var dSemana = document.createElement('ul');
    dSemana.className = 'cld-labels';
    var dlista = ["D", "L", "M", "X", "J", "V", "S"];
    for(var i = 0; i < dlista.length; i++){
      var label = document.createElement('li');
      label.className += "cld-label";
      label.innerHTML = dlista[i];
      dSemana.appendChild(label);
    }

    mainSeccion.appendChild(dSemana);
  }
  function AgragarDias(){
    // Crear el numero de elemento
    function NumeroDia(n){
      var numero = document.createElement('p');
      numero.className += "cld-numero";
      numero.innerHTML += n;
      return numero;
    }
    var dias = document.createElement('ul');
    dias.className += "cld-dias";
    // Dias Previoios
    for(var i = 0; i < (calendario.seleccionado.PrimerDia); i++){
      var dia = document.createElement('li');
      dia.className += "cld-dia PrevMes";
      var numero = NumeroDia((calendario.Previo.Dias - calendario.seleccionado.PrimerDia) + (i+1));
      dia.appendChild(numero);
      dias.appendChild(dia);
    }
    // Mes y dia actual (hoy)
    for(var i = 0; i < calendario.seleccionado.Dias; i++){
      var dia = document.createElement('li');
      dia.className += "cld-dia actualMes";


      var numero = NumeroDia(i+1);
      // Revisar Eventos
      for(var n = 0; n < calendario.Modelo.length; n++){
//console.log(calendar.Model)
        //Eventos del mes
        var eventoMes = calendario.Modelo[n].Date;
        var alDia = new Date(calendario.seleccionado.Ano, calendario.seleccionado.Mes, (i+1));
        if(eventoMes.getTime() == alDia.getTime()){
          numero.className += " diaevento";
          var title = document.createElement('span');
          title.className += "cld-title";

          numero.appendChild(title);
          console.log(eventoMes)
        }
      }
      dia.appendChild(numero);
      // Es hoy?
      if((i+1) == calendario.Hoy.getDate() && calendario.seleccionado.Mes == calendario.Hoy.Mes && calendario.seleccionado.Ano == calendario.Hoy.Ano){
        dia.className += " hoy";

      }
      dias.appendChild(dia);
    }

    var diasExtras = 13;
    if(dias.children.length>35){diasExtras = 6;}
    else if(dias.children.length<29){diasExtras = 20;}

    for(var i = 0; i < (diasExtras - calendario.seleccionado.UltimoDia); i++){
      var dia = document.createElement('li');
      dia.className += "cld-dia ProxtMes";

      var numero = NumeroDia(i+1);
      dia.appendChild(numero);
      dias.appendChild(dia);
    }
    mainSeccion.appendChild(dias);
    //console.log(days)
  }
  elemento.appendChild(mainSeccion);

//Agregar Cabecera
  AgregarFecha();
// Agregar Dias de la semana
  AgregarDiaSemana();
// Agregar Dias
  AgragarDias();
}

function calendario(el, data){
  var obj = new Calendario(data);
  //console.log(el)
  //console.log(data)
  crearCalendario(obj, el);
}
